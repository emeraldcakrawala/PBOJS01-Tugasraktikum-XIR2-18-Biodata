package biodatadiri;

import java.io.BufferedReader;
import java.io.InputStreamReader;

class Biodata { 
    public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader (new InputStreamReader (System.in));
        
    //instance of class
    Biodatadiri bd = new Biodatadiri();
    //input
    System.out.print("Masukkan Nama Siswa : ");
    String nama_bd = br.readLine();
    System.out.print("Masukkan NIS Siswa : ");
    int nis_bd = Integer.parseInt(br.readLine());
    System.out.print("Masukkan Jenis Kelamin Siswa : ");
    String jenis_bd = br.readLine();
    System.out.print("Masukkan Tempat Lahir Siswa : ");
    String tempat_bd = br.readLine();
    System.out.print("Masukkan Alamat Siswa : ");
    String alamat_bd = br.readLine();
    System.out.print("Masukkan Tanggal Lahir Siswa : ");
    String tanggal_bd = br.readLine();
    System.out.print("Masukkan Moto Siswa : ");
    String moto_bd = br.readLine();
    //setter
    bd.setNama(nama_bd);
    bd.setNis(nis_bd);
    bd.setTmpt(tempat_bd);
    bd.setJk(jenis_bd);
    bd.setAlamat(alamat_bd);
    bd.setTanggal(tanggal_bd);
    bd.setMoto(moto_bd);
    //output
    System.out.println("=================");
    System.out.println("  BIODATA SISWA  ");
    System.out.println("=================");
    System.out.println("Nama Siswa = "+bd.getNama());
    System.out.println("NIS Siswa = "+bd.getNis());
    System.out.println("Tempat Lahir Siswa = "+bd.getTmpt());
    System.out.println("Jenis Kelamin Siswa = "+bd.getJk());
    System.out.println("Alamat Siswa = "+bd.getAlamat());
    System.out.println("Tanggal Lahir Siswa = "+bd.getTanggal());
    System.out.println("Moto Siswa = "+bd.getMoto());
    }
    }
