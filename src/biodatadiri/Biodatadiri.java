package biodatadiri;
public class Biodatadiri {
    //deklarasi
    private String nama, tmptlahir, tgllahir, jk, alamat, moto;
    private int nis;
    //setter
    public void setNama(String nama){
        this.nama= nama;
    }
    public void setTmpt(String tmptlahir){
        this.tmptlahir= tmptlahir;
    }
    public void setTanggal(String tgllahir){
        this.tgllahir= tgllahir;
    }
    public void setJk(String jk){
        this.jk= jk;
    }
    public void setAlamat(String alamat){
        this.alamat= alamat;
    }
    public void setMoto(String moto){
        this.moto= moto;
    }
    public void setNis(int nis){
        this.nis= nis;
    }
    //getter
    public String getNama(){
        return nama;
    }
    public String getTmpt(){
        return tmptlahir;
    }
    public String getTanggal(){
        return tgllahir;
    }
    public String getJk(){
        return jk;
    }
    public String getAlamat(){
        return alamat;
    }
    public String getMoto(){
        return moto;
    }
    public int getNis(){
        return nis;
    }
}